#include <stdio.h>
#include <math.h>

//0,9904 m / 258s
//const double a = 0.10; //m
const double a = 100.0; //mm
//const double b = 0.10; //m
const double b = 100.0; //mm
//const double abflussAbstand = 0.1; //m
const double abflussAbstand = 100.0; //mm

//const double zuflussRate = 0.01/60.0; // m³/s
const double zuflussRate = 10000000.0/60.0; // mm³/s
const double abflussRate = 1.0/60.0; // l/s

//const double maxFH = 1.0; //m
const double maxFH = 1000.0; //mm
//double inhalt = 0.0; //m³
double inhalt = 0.0; //mm³
double time = 0.0; // seconds

double Max_FH_VALUE = 0.0;

double simulationStepTime = 0.001; // sekunden
int simulationStep = 0;

//const double lochGroesse = 0.000000005321257; // m²
double lochGroesse = 5.321257; // mm²

int simulationRound();
double lochGroesseBerechnen();
double flussBerechnen(double lochGrInQM, double hoeheInM);

int main() {
    lochGroesse=lochGroesseBerechnen();
    while(simulationRound()==0);
    printf("Simulation ran");
    return 0;
}


int simulationRound() {
    double FH = 0.0; // mm
    FH = (inhalt / (a*b));
    if(FH > Max_FH_VALUE)
        Max_FH_VALUE = FH;
    int anzahlLoecher = 1 + ((int)(FH / abflussAbstand));
    double abflussRateGes = 0.0; // mm³/s

    for(int i = 0; i < anzahlLoecher; i++){
        double lochWasserstand = (FH) - (i*abflussAbstand);
        abflussRateGes += flussBerechnen(lochGroesse, lochWasserstand);
    }


    double abgang = abflussRateGes * simulationStepTime; //m³
    double zugang = zuflussRate * simulationStepTime;


    inhalt -= abgang;
    inhalt += zugang;

    simulationStep++;
    //printf("Zugang: %f \n Abgang: %f",zugang, abgang);
    //if(fabs(abgang - zugang) >= 10.0 ){ //todo präzision des Vergleichs  > 0.00000001
    if(zugang-abgang >= 1.0 ){ //todo präzision des Vergleichs  > 0.00000001
    //if(FH < 900.0){
        return 0;//simulationRound();
    }
    else{
        printf("TimeSteps: %d \n", simulationStep);
        printf("Time (Seconds): %f \n", simulationStep * simulationStepTime);
        printf("Füllhöhe: %f mm \n",FH);
        printf("Max Füllhöhe: %f", Max_FH_VALUE);
        return 1;
    }

}



//phi = Al * vl : Fläche des Loches * Fließgeschwindigkeit
//vl = wurzel(2*g*hB)   g = 9.81 m/s²
//lochgröße berechnen:
//ganz voll (FH = 100cm): das 5te (index 4) loch führt 1l / min ab

// Ekin = 1/2 * mL * v²

double lochGroesseBerechnen() {
    //5,321257 QuadratMillimeterlochGroesseBerechnen
    double lochgr=(1.0/(1000.0*60.0))/sqrt(2.0*9.81*0.50) * 1000000.0;
    printf("Lochgroesse %f \n",lochgr); //50cm sind 0.5 m   l/min zu m³/s => /1000 /60 => / (1000*60)   m² zu mm² : * 10⁶
    return lochgr;
}

double flussBerechnen(double lochGrInQMM, double hoeheInMM){
    //phi = Al * vl = mm³/s
    double phi = lochGrInQMM * sqrt(2.0 * 9810.0 * hoeheInMM);
    return phi;
}

/*double flussBerechnen(double lochGrInQM, double hoeheInM){
    //phi = Al * vl = m³/s
    double phi = lochGrInQM * sqrt(2.0 * 9.81 * hoeheInM);
    return phi;
}
 */

